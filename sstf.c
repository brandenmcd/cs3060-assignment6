/* Promise of Originality
I promise that this source code file has, in it's entirety, been
written by myself or a member of my group and by no other person or
persons. If at any time an exact copy of this source code is found to
be used by another person outside my group in this term, I understand
that all members of my group and the members of the group that
submitted the copy will receive a zero on this assignment.
*/

#include<stdio.h>
#include<string.h>
#include "assn6.h"

#define MAX_SIZE 100
#define TRUE 1
#define FALSE 0
#define RRTIME 10
#define DONE 0

void shortestSeekTimeFirst(int *blockValue,int count)
{
	int temp[count];
    int currentLowest = 0;
    int difference = 0;
    int nextIndex =0;
    int testCase = 0;
    int total = 0;
    int bit = 0;
    int i;
    int l;

    for (i = 0; i < count; i++)
    {
        temp[i] = 0;
    }
    temp[0] = 1;

    for (l = 0; l < count; l++)
    {    
        for (i = 0; i < count; i++)
        {
            if (blockValue[testCase] > blockValue[i] && temp[i] != 1)
            {
                difference = blockValue[testCase] - blockValue[i];
                if (bit == 0)
                {
                    currentLowest = difference;
                    nextIndex = i;
                    bit = 1;
                }                        
            }
            else if (temp[i] != 1)
            {
                difference = blockValue[i] - blockValue[testCase];
                if (bit == 0)
                {
                    currentLowest = difference;
                    nextIndex = 1;
                    bit = 1;
                }
            }
            if (currentLowest > difference && temp[i] != 1 )
            {
                currentLowest = difference;
                nextIndex = i;
            }

        }
            testCase = nextIndex;
            temp[testCase] = 1;
            total += currentLowest;
            currentLowest = 0;
            bit = 0;
    }

	printf("SSFT Total Seek: %d\n", total);
		
}