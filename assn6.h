/* Promise of Originality
I promise that this source code file has, in it's entirety, been
written by myself or a member of my group and by no other person or
persons. If at any time an exact copy of this source code is found to
be used by another person outside my group in this term, I understand
that all members of my group and the members of the group that
submitted the copy will receive a zero on this assignment.
*/

//constants
#define MAX_SIZE 100
#define TRUE 1
#define FALSE 0
#define RRTIME 10
#define DONE 0


//function declarations
void firstComeFirstServe(int* blocks, int count);
void shortestSeekTimeFirst(int* blocks, int count);
void nonCircularLook(int* blocks, int count);
void circularLook(int* blocks, int count);

int compareAsc (const void* left, const void* right);
int compareDesc (const void* left, const void* right);
int Diff(int first, int second);
