/* Promise of Originality
I promise that this source code file has, in it's entirety, been
written by myself or a member of my group and by no other person or
persons. If at any time an exact copy of this source code is found to
be used by another person outside my group in this term, I understand
that all members of my group and the members of the group that
submitted the copy will receive a zero on this assignment.
*/
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include "assn6.h"


void circularLook(int *blocks, int count)
{ 
	//Arrays
	int initArray[count];
	int sortedArray[count];
  
	//Integers
	int totalTime = 0;
	int indx = 0;
	int iterator = blocks[0];
	int way = 1;
    int i;
	
	//Copy to sortedArray
	for (i = 0; i < count; i++)
	{
		sortedArray[i] = blocks[i];
	}
	
	qsort(sortedArray, count, sizeof(int), compareAsc);
  
	for (i = 0; i < count; i++)
	{
		initArray[i] = 0;
		if (iterator == sortedArray[i])
		{
		  initArray[i] = 1;
		  indx = i;
		}
    }
  
    for (i = 0; i < count-1; i++)
    {
		while (initArray[indx] == 1)
		{
		  indx += way;
		  if(indx > count-1)
		  {
			indx = 0;
		  }
		}
		totalTime += Diff(iterator, sortedArray[indx]);
		initArray[indx] = 1;
		iterator = sortedArray[indx];
	}
	
	printf("C-LOOK Total Seek: %d\n", totalTime);
}
