/* Promise of Originality
I promise that this source code file has, in it's entirety, been
written by myself or a member of my group and by no other person or
persons. If at any time an exact copy of this source code is found to
be used by another person outside my group in this term, I understand
that all members of my group and the members of the group that
submitted the copy will receive a zero on this assignment.
*/

#include <stdlib.h>
#include<stdio.h>
#include<limits.h>
#include "assn6.h"

#define MAX_SIZE 100

int main()
{
	int blocks[MAX_SIZE];
	int count = 0;
	
	//read in the array and save to variables that get passed into the functions
	while(count < MAX_SIZE && fscanf(stdin, "%d", &blocks[count]) > 0)
	{		
	    	count++;	
	};	
	printf("Assignment 6: Block Access Alogrithm\n");	
	printf("By: Daniel Burgener\nEric Draper\nBranden McDonald\n\n");
	//these will each print their results
	firstComeFirstServe(blocks, count);
	shortestSeekTimeFirst(blocks, count);
	nonCircularLook(blocks, count);
	circularLook(blocks, count);
	
	return 0;
}
