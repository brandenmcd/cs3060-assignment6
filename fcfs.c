/* Promise of Originality
I promise that this source code file has, in it's entirety, been
written by myself or a member of my group and by no other person or
persons. If at any time an exact copy of this source code is found to
be used by another person outside my group in this term, I understand
that all members of my group and the members of the group that
submitted the copy will receive a zero on this assignment.
*/

#include<stdio.h>
#include<string.h>
#include "assn6.h"

void firstComeFirstServe(int *start, int count)
{
	int total = 0;
	int i;
	for(i =0;i < count -1; i++)
	{
		if (start[i] > start[i+1])
		{
			
			total += start[i] - start[i+1];
		}
		else
		{
		
			total += start[i+1] - start[i];
			
		}
	}
	
	printf("FCFS Total Seek: %d\n", total);		
}