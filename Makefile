assn6: assn6.o fcfs.o sstf.o ncl.o cl.o Makefile
	gcc -g -Werror -Wextra -Wall -o assn6 assn6.o fcfs.o sstf.o ncl.o cl.o
	
assn6.o: assn6.c assn6.h Makefile
	gcc -c -g -Werror -Wextra -Wall -o assn6.o assn6.c 

fcfs.o: fcfs.c assn6.h Makefile
	gcc -c -g -Werror -Wextra -Wall -o fcfs.o fcfs.c	
	
sstf.o: sstf.c assn6.h  Makefile
	gcc -c -g -Werror -Wextra -Wall -o sstf.o sstf.c

ncl.o: ncl.c assn6.h Makefile
	gcc -c -g -Werror -Wextra -Wall -o ncl.o ncl.c
	
cl.o: cl.c assn6.h Makefile
	gcc -c -g -Werror -Wextra -Wall -o cl.o cl.c	
	

clean:
	rm assn6.o fcfs.o sstf.o ncl.o cl.o assn6
