/* Promise of Originality
I promise that this source code file has, in it's entirety, been
written by myself or a member of my group and by no other person or
persons. If at any time an exact copy of this source code is found to
be used by another person outside my group in this term, I understand
that all members of my group and the members of the group that
submitted the copy will receive a zero on this assignment.
*/

#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include "assn6.h"

int compareAsc (const void* left, const void* right){
	return (*(int*)left - *(int*)right);
}

int compareDesc (const void* left, const void* right){
	return (*(int*)right - *(int*)left);
}

int Diff(int first, int second){
	if (first < second){
		return second-first;
	}
	else{
		return first-second;
	}
}

void nonCircularLook(int* blocks, int count){
  int i;
  int time = 0;
  int read[count];
  int nextIndex = 0;
  int current = blocks[0];
	int sortedBlocks[count];
	int direction = -1;//Start going down
	//Sort the blocks array
	for (i = 0; i < count; i++){
		sortedBlocks[i] = blocks[i];
	}
	qsort(sortedBlocks, count, sizeof(int), compareDesc);
  //Set read to all 0
  for (i = 0; i < count; i++){
    read[i] = 0;
		if (current == sortedBlocks[i]){
			read[i] = 1;
			nextIndex = i;
		}
  }
	for (i = 0; i < count-1; i++){
    while (read[nextIndex] == 1){
      nextIndex += direction;
      if (nextIndex < 0){
        nextIndex = 0;
        direction = 1;
      }
      else if(nextIndex > count-1){
        nextIndex = count-1;
        direction = -1;
      }
    }
    time += Diff(current, sortedBlocks[nextIndex]);
    read[nextIndex] = 1;
    current = sortedBlocks[nextIndex];
  }

  printf("LOOK Total Seek: %d\n", time);
}



